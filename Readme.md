# Small Endless Match-3 video game

This is a small match3 clone video game. I wrote this for fun and learning purposes in vanilla JavaScript. Enjoy!

[Play Online](https://palma3000.itch.io/simple-endless-match-3)

# License

```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004
 
Copyright (C) 2023 Palma3000 <palma3000@airmail.cc>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
```
